<?php
/**
 * @file
 * Support for role permissions.
 */

/**
 * Destination class implementing migration into role permissions.
 */
class MigrateDestinationRolePermission extends MigrateDestination {

  /**
   * The role.
   *
   * @var string
   */
  protected $role;
  /**
   * Function to get the role.
   */
  public function getRole() {
    return $this->role;
  }

  /**
   * Return the name of the class.
   */
  public function __toString() {
    return t('Role Permission');
  }

  /**
   * Initiate the role_permissions for a particular role.
   *
   * @param string $role_name
   *   The name of the role.
   */
  public function __construct($role_name) {
    $this->role = user_role_load_by_name($role_name);
  }

  /**
   * Set the primary key schema for role permissions.
   *
   * @return array
   *   Return the schema for the key.
   */
  static public function getKeySchema() {
    return array(
      'rid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'ID of destination role',
      ),
      'permission' => array(
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
        'description' => 'key of the destination permission',
      ),
    );
  }

  /**
   * Import a single row.
   *
   * @param object $object
   *   Object object to build. Prefilled with any fields mapped in the
   *   Migration.
   * @param object $row
   *   Raw source data object - passed through to prepare/complete handlers.
   * 
   * @return array
   *   Array of key fields of the object that was saved if
   *   successful. FALSE on failure.
   */
  public function import(stdClass $object, stdClass $row) {
    $modules = user_permission_get_modules();

    migrate_instrument_start('role_permission_grant');

    if ($modules[$object->permission]) {
      user_role_grant_permissions($this->role->rid, array($object->permission));

      migrate_instrument_stop('role_permission_grant');
      return array('rid' => $this->role->rid, 'permission' => $object->permission);
    }
    else {
      // Permission does not exist. Consider this a success.
      migrate_instrument_stop('role_permission_grant');
      return array('rid' => $this->role->rid, 'permission' => $object->permission);
    }

    migrate_instrument_stop('role_permission_grant');
    return FALSE;
  }

  /**
   * Delete a migrated permission.
   *
   * @param array $ids
   *   Array of fields representing the key.
   */
  public function rollback(array $ids) {
    migrate_instrument_start('role_permission_delete');

    user_role_revoke_permissions($ids['destid1'], array($ids['destid2']));

    migrate_instrument_stop('role_permission_delete');

    return TRUE;
  }

  /**
   * Returns a list of fields available to be mapped.
   *
   * @param Migration $migration
   *   Optionally, the migration containing this destination.
   * 
   * @return array
   *   Keys: machine names of the fields (to be passed to addFieldMapping)
   *   Values: Human-friendly descriptions of the fields.
   */
  public function fields($migration = NULL) {
    $fields = array();

    $fields['rid'] = t('Existing Role ID');
    $fields['permission'] = t('Permission Key');
    $fields['module'] = t('Module');

    return $fields;
  }
}
