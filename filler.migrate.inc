<?php
/**
 * @file
 * Migrate file for filler.
 */

/**
 * Implements hook_migrate_api().
 * 
 * Actual implementation is done via hook_flush_cache().
 */
function filler_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'term' => array(
        'title' => t('Taxonomy Terms'),
      ),
      'role_permission' => array(
        'title' => t('Role Permissions'),
      ),
    ),
  );
  return $api;
}
