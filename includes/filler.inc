<?php

/**
 * @file
 * Generic migration class for Filler.
 */

class Filler extends Migration {

  /**
   * Constructor function.
   */
  public function __construct($arguments, $destination_class, $destination_args) {
    parent::__construct($arguments);

    // Add the source as the CSV file.
    $source_options = array(
      'header_rows' => 1,
      'track_changes' => !empty($arguments['source_track_changes']),
    );
    $this->source = new MigrateSourceCSV($arguments['source_file'], array(), $source_options);

    // Add the destination as the proper entity type.
    // We need to use a reflection class so we can pass in the arguments.
    $class = new ReflectionClass($destination_class);
    $this->destination = $class->newInstanceArgs($destination_args);

    // Handle CSV specific field mappings.
    $this->map = new MigrateSQLMap($this->machineName, array(
      'id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      ), $destination_class::getKeySchema()
    );

    // Handle default fields and custom fields.
    $fields = array_keys($this->destination->fields());

    foreach ($this->source->csvcolumns as $key => $value) {
      $field = $value[0];
      if (in_array($field, $fields)) {
        $mapping = $this->addFieldMapping($field, $field);
        // Handle multivalue fields by comma separator.
        if (strpos($field, 'field_') === 0) {
          $field_info = field_info_field($field);
          if ($field_info['cardinality'] <> 1) {
            $mapping->separator(',');
          }
          // Need to handle more complex fields in a different way here.
        }
      }
    }
  }
}
