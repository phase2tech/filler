<?php

/**
 * @file
 * Role Permission migration class for Filler.
 */

class FillerRolePermission extends Filler {

  /**
   * Constructor function.
   */
  public function __construct($arguments) {
    parent::__construct($arguments, 'MigrateDestinationRolePermission', array($arguments['role']));
  }
}
