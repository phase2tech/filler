<?php

/**
 * @file
 * Taxonomy Term migration class for Filler.
 */

class FillerTerm extends Filler {

  /**
   * Constructor function.
   */
  public function __construct($arguments) {
    parent::__construct($arguments, 'MigrateDestinationTerm', array($arguments['vocabulary']));

    $this->addFieldMapping('parent', 'pid')
      ->sourceMigration($this->machineName);
  }

}
